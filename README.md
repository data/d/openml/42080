# OpenML dataset: federal_election

https://www.openml.org/d/42080

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

General Description
2015-current: greater than $200.00. The Commission categorizes contributions from individuals using the calendar year-to-date amount for political action committee (PAC) and party committee receipts and the election-cycle-to-date amount for candidate receipts to determine whether the contribution meets the categorization threshold of greater than $200.00.

1989-2014: $200 and above. The Commission categorized contributions from individuals using the reporting period amount to determine whether a contribution met the categorization threshold of $200.00 or more.

1975-1988: $500 and above. The Commission categorized contributions from individuals using the reporting period amount to determine whether a contribution met the categorization threshold of $500.00 or more.

header description can be found here : https://classic.fec.gov/finance/disclosure/metadata/DataDictionaryContributionsbyIndividuals.shtml

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42080) of an [OpenML dataset](https://www.openml.org/d/42080). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42080/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42080/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42080/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

